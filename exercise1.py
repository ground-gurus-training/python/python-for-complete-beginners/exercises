def list_celebrities(celebs):
    for celebrity in celebs:
        print(f"{celebrity.get('firstName')} {celebrity.get('lastName')}, {celebrity.get('gender')}")


def filter_celebrities(celebs, gender):
    filtered_celebs = []
    for celebrity in celebs:
        if gender == celebrity.get('gender'):
            filtered_celebs.append(celebrity)
    return filtered_celebs


if __name__ == '__main__':
    celebrities = [
        {
            "firstName": "Jessy",
            "lastName": "Mendiola",
            "gender": "F"
        },
        {
            "firstName": "Aga",
            "lastName": "Mulac",
            "gender": "M"
        }
    ]
    filtered = filter_celebrities(celebrities, 'F')
    list_celebrities(filtered)
