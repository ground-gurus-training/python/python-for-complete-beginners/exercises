result = 0
for num in range(1, 101):
    if num % 2 == 0:
        result += num
print(f"The sum is {result}")
